local PART={}
PART.ID = "timmybigleverswitch4"
PART.Name = "timmybigleverswitch4"
PART.Model = "models/Timmy2985/Tardis/Interior/bigleverswitch4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
	function PART:Use()
	local exterior=self.exterior
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
			if exterior:GetData("vortex") then
				exterior:Mat()
			end
		else
			self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
			exterior:Demat()
		end
	end
end

TARDIS:AddPart(PART)