local PART={}
PART.ID = "timmygraylever5"
PART.Name = "timmygraylever5"
PART.Model = "models/Timmy2985/Tardis/Interior/graylever5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 6

if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_dial.wav" ))
	end
end

TARDIS:AddPart(PART,e)