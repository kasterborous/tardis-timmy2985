local PART={}
PART.ID = "timmybigleverswitch2"
PART.Name = "timmybigleverswitch2"
PART.Model = "models/Timmy2985/Tardis/Interior/bigleverswitch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
	function PART:Use()
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
		local exterior=self.exterior
			exterior:ToggleLocked()
	end
end

TARDIS:AddPart(PART,e)