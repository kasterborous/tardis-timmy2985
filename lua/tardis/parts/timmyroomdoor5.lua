local PART={}
PART.ID = "timmyroomdoor5"
PART.Name = "timmyroomdoor5"
PART.Model = "models/Timmy2985/Tardis/Interior/roomdoor5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1


if SERVER then
	function PART:Collide()
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
	end

	function PART:DontCollide()
		self:SetCollisionGroup(COLLISION_GROUP_WORLD)
	end

	function PART:Use()
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_doorclose.wav" ))
			self:Collide( true )
		else
			self:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_dooropen.wav" ))
			self:DontCollide( true )
		end
		

	end
end


TARDIS:AddPart(PART,e)