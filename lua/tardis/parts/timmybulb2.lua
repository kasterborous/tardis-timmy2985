local PART={}
PART.ID = "timmybulb2"
PART.Name = "timmybulb2"
PART.Model = "models/Timmy2985/Tardis/Interior/bulb2.mdl"
PART.AutoSetup = true

if CLIENT then
	function PART:Think()
		local switch = TARDIS:GetPart(self.interior,"timmytinytinyswitch10")
		if ( switch:GetOn() ) then
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1on")
		else
			self:SetMaterial("models/Timmy2985/Tardis/Interior/bulb1off")
		end
	end
end

TARDIS:AddPart(PART,e)