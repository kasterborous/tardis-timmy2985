local PART={}
PART.ID = "timmybigleverswitch1"
PART.Name = "timmybigleverswitch1"
PART.Model = "models/Timmy2985/Tardis/Interior/bigleverswitch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2

if SERVER then
	function PART:Use(activator)
		local entrencedoor = TARDIS:GetPart(self.interior,"timmyentrencedoor")
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_lever.wav" ))
		entrencedoor:Toggle( !entrencedoor:GetOn(), activator )
	end
end

TARDIS:AddPart(PART,e)