local PART={}
PART.ID = "timmygraylever3"
PART.Name = "timmygraylever3"
PART.Model = "models/Timmy2985/Tardis/Interior/graylever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 6


if SERVER then
	function PART:Use( ply )
	local exterior=self.exterior
	local extdoors=exterior:GetPart("door")
		if ( self:GetOn() ) then
			self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_dial.wav" ))
		ply:ChatPrint("TARDIS Cloaking Device Deactivated")
		exterior:SetColor (Color(73,0,91,55))
		extdoors:SetColor (Color(73,0,91,55))
		exterior:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_uncloak.wav" ))
			timer.Create( "uncloaktimer" , 1 , 1 , function()
			exterior:SetColor (Color(255,255,255,255))
			exterior:SetRenderMode( RENDERMODE_NORMAL )
			extdoors:SetColor (Color(255,255,255,255))
			extdoors:SetRenderMode( RENDERMODE_NORMAL )
			end )
		else
			self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_dial.wav" ))
		ply:ChatPrint("TARDIS Cloaking Device Activated")
		exterior:SetRenderMode( RENDERMODE_TRANSALPHA )
		exterior:SetColor (Color(73,0,91,55))
		extdoors:SetRenderMode( RENDERMODE_TRANSALPHA )
		extdoors:SetColor (Color(73,0,91,55))
		exterior:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_cloak.wav" ))
			timer.Create( "cloaktimer" , 1 , 1 , function()
			exterior:SetRenderMode( RENDERMODE_TRANSALPHA )
			exterior:SetColor (Color(255,255,255,0))
			extdoors:SetRenderMode( RENDERMODE_TRANSALPHA )
			extdoors:SetColor (Color(255,255,255,0))

			end )
		end
 	end
end

TARDIS:AddPart(PART,e)