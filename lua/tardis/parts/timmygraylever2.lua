local PART={}
PART.ID = "timmygraylever2"
PART.Name = "timmygraylever2"
PART.Model = "models/Timmy2985/Tardis/Interior/graylever2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 6

if SERVER then
	function PART:Use(activator)
            self:EmitSound("timmy2985/tardis/sounds/timmy_dial.wav")
            		self.exterior:ToggleDoor()
        end
end

TARDIS:AddPart(PART,e)