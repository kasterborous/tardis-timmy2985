local PART={}
PART.ID = "timmysmallswitch3"
PART.Name = "timmysmallswitch3"
PART.Model = "models/Timmy2985/Tardis/Interior/smallswitch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 8


if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "timmy2985/tardis/sounds/timmy_littleswitch.wav" ))
	end
end

TARDIS:AddPart(PART,e)