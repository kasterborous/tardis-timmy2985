local PART={}
PART.ID = "timmyphone"
PART.Name = "timmyphone"
PART.Model = "models/Timmy2985/Tardis/Interior/phone.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 5

if SERVER then
	function PART:Use(activator)
		self:EmitSound( Sound( "Timmy2985/tardis/sounds/timmy_phone.wav" ))
	end
end

TARDIS:AddPart(PART,e)